package id.mydev.mongodb.repository;

import id.mydev.mongodb.dto.UserDto;
import id.mydev.mongodb.model.Chat;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface ChatRepository extends MongoRepository<Chat, String> {
    List<Chat> findChatByChatToAndChatStatus(UserDto chatTo, Integer chatStatus);

    List<Chat> findChatByContentContainsAndChatStatus(String content, Integer chatStatus);
}
