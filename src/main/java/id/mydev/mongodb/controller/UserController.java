package id.mydev.mongodb.controller;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;

import id.mydev.mongodb.dto.RequestNewUserDto;
import id.mydev.mongodb.service.UserService;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping(value = "/user", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;
    @GetMapping("/getAll")
    public ResponseEntity<String> getAllMerchantCategory() throws JsonProcessingException {
        return userService.getUserList();
    }
    @GetMapping("/getUserById/{userId}")
    public ResponseEntity<String> findById(@PathVariable String userId) throws JsonProcessingException {
        return userService.findById(userId);
    }
    @GetMapping("/getUserByUsername/{username}")
    public ResponseEntity<String> findByUsername(@PathVariable String username) throws JsonProcessingException {
        return userService.findByUsername(username);
    }
    @PostMapping("/addNewUser")
    public ResponseEntity<String> getAllMerchantCategory(@RequestBody RequestNewUserDto param) throws JsonProcessingException {
        return userService.addNewUser(param);
    }

}
