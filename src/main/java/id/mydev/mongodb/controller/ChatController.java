package id.mydev.mongodb.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import id.mydev.mongodb.dto.RequestNewChat;
import id.mydev.mongodb.dto.UserDto;
import id.mydev.mongodb.service.ChatService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/chat")
@RequiredArgsConstructor
public class ChatController {
    private final ChatService chatService;
    @PostMapping("/getChatTo")
    public ResponseEntity<String> getChatTo(@RequestBody UserDto chatTo) throws JsonProcessingException {
        return chatService.getChatTo(chatTo);
    }
    @GetMapping("/getChatByContent")
    public ResponseEntity<String> getChatByContent(@RequestParam(required = false, name = "content") String content) throws JsonProcessingException {
        return chatService.getChatByContent(content);
    }
    @PostMapping("/addNewChat")
    @MessageMapping("/news")
    @SendTo("/topic/news")
    public ResponseEntity<String> getChatTo(@RequestBody RequestNewChat params) throws JsonProcessingException {
        return chatService.addNewChat(params);
    }
}

/*


* */
