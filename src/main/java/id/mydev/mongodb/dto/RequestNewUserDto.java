package id.mydev.mongodb.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class RequestNewUserDto {
    private String userFirstName;
    private String userLastName;
    private String userUsername;
    private String userPass;
    private String userConfirmPass;
}
