package id.mydev.mongodb.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class ChatToDto {
    private String chatId;
    private UserDto chatFrom;
    private UserDto chatTo;
    private String content;
    private String attachment;
    private Integer chatStatus;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="Asia/Jakarta")
    private Date createdDate;}
