package id.mydev.mongodb.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class RequestNewChat {
    private UserDto chatFrom;
    private UserDto chatTo;
    private String content;
    private String attachment;
}