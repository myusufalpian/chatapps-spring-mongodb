package id.mydev.mongodb.service;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;

import id.mydev.mongodb.dto.RequestNewUserDto;
import id.mydev.mongodb.dto.UserDto;
import id.mydev.mongodb.model.User;
import id.mydev.mongodb.repository.UserRepository;
import id.mydev.mongodb.util.GenerateResponse;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final ModelMapper mapper;
    public ResponseEntity<String> getUserList() throws JsonProcessingException{
        Collection<User> users = userRepository.findAll();
        return GenerateResponse.success("Get data user success", users.stream().map(x -> mapper.map(x, UserDto.class)).toList());
    }

    public ResponseEntity<String> findById(String userId) throws JsonProcessingException{
        Optional<User> user = userRepository.findById(userId);
        return user.isEmpty() ? GenerateResponse.notFound("User not found!", null) : GenerateResponse.success("get data user success", mapper.map(user, UserDto.class));
    }

    public ResponseEntity<String> findByUsername(String username) throws JsonProcessingException{
        Optional<User> user = userRepository.findByUserUsername(username);
        return user.isEmpty() ? GenerateResponse.notFound("User not found!", null) : GenerateResponse.success("get data user success", mapper.map(user, UserDto.class));
    }

    @Transactional
    public ResponseEntity<String> addNewUser(RequestNewUserDto params) throws JsonProcessingException{
        Optional<User> checkUser = userRepository.findByUserUsername(params.getUserUsername());
        if (checkUser.isPresent()) return GenerateResponse.badRequest("Username is already taken", null);
        else if (!params.getUserPass().equals(params.getUserConfirmPass())) return GenerateResponse.badRequest("Username is already taken", null);
        User user = mapper.map(params, User.class);
        user.setUserId(UUID.randomUUID().toString());
        user.setCreatedDate(new Date());
        userRepository.save(user);
        return GenerateResponse.success("Add new user success", null);
    }

}
