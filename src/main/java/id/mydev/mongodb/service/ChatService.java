package id.mydev.mongodb.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import id.mydev.mongodb.dto.ChatToDto;
import id.mydev.mongodb.dto.RequestNewChat;
import id.mydev.mongodb.dto.UserDto;
import id.mydev.mongodb.model.Chat;
import id.mydev.mongodb.model.User;
import id.mydev.mongodb.repository.ChatRepository;
import id.mydev.mongodb.repository.UserRepository;
import id.mydev.mongodb.util.GenerateResponse;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
public class ChatService {
    private final ChatRepository chatRepository;
    private final UserRepository userRepository;
    private final ModelMapper mapper;
    public ResponseEntity<String> getChatTo(UserDto chatTo) throws JsonProcessingException {
        List<Chat> chats = chatRepository.findChatByChatToAndChatStatus(chatTo, 0);
        return GenerateResponse.success("Get data chat with "+ chatTo.getUserFirstName() + " " + chatTo.getUserLastName() +" success", chats);
    }

    public ResponseEntity<String> getChatByContent(String content) throws JsonProcessingException {
        List<Chat> chats = chatRepository.findChatByContentContainsAndChatStatus(content, 0);
        return GenerateResponse.success("Get data chat by content success", chats);
    }

    public ResponseEntity<String> addNewChat(RequestNewChat params) throws JsonProcessingException {
        Optional<User> checkSender = userRepository.findById(params.getChatFrom().getUserId());
        if (checkSender.isEmpty()) return GenerateResponse.notFound("user sender not found", null);
        Optional<User> checkReceiver = userRepository.findById(params.getChatTo().getUserId());
        if (checkReceiver.isEmpty()) return GenerateResponse.notFound("user receive message not found", null);
        Chat chat = new Chat();
        chat.setChatId(UUID.randomUUID().toString());
        chat.setChatFrom(params.getChatFrom());
        chat.setChatTo(params.getChatTo());
        chat.setContent(params.getContent());
        chat.setAttachment(params.getAttachment());
        chat.setChatStatus(0);
        chat.setCreatedDate(new Date());
        chatRepository.save(chat);
        return GenerateResponse.success("Add new chat with " + params.getChatTo().getUserFirstName() + " " + params.getChatTo().getUserLastName() + " success", null);
    }
}
